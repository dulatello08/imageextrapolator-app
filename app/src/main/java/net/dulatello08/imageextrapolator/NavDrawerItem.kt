package net.dulatello08.imageextrapolator

sealed class NavDrawerItem(var route: String, var icon: Int, var title: String) {
    object Home : NavDrawerItem("home", R.drawable.ic_home, "Home")
    object Settings : NavDrawerItem("settings", R.drawable.ic_settings, "Settings")
}