package net.dulatello08.imageextrapolator

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody


class HomeViewModel(private val applicationContext: Context): ViewModel() {
    val image: MutableLiveData<ByteArray> by lazy { MutableLiveData<ByteArray>() }
    val resultImage: MutableLiveData<Bitmap> by lazy { MutableLiveData<Bitmap>() }
    fun extrapolateImage() {
        val result: Deferred<ByteArray> = viewModelScope.async {
            val retrofit = ApiClient.setupRetrofit(applicationContext)
            val responseBody = retrofit.extrapolateImage(RequestBody.create(MediaType.parse("image/jpeg"), image.value!!)).body()
            responseBody!!.bytes()
        }
        viewModelScope.launch {
            val syncResult = result.await()
            resultImage.value = BitmapFactory.decodeByteArray(syncResult, 0, syncResult.size)
        }
    }
}
