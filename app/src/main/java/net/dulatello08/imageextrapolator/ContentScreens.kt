package net.dulatello08.imageextrapolator

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.Settings
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File

private val openPermDialog = mutableStateOf(false)
private val imageResult = mutableStateOf(Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888))

@ExperimentalPermissionsApi
@Composable
fun HomeScreen() {
    val context = LocalContext.current
    val viewModel: HomeViewModel = HomeViewModel(context)
    val coroutineScope = rememberCoroutineScope()
    val readExternalStorageState: PermissionState = rememberPermissionState(permission = Manifest.permission.READ_EXTERNAL_STORAGE)
    if(openPermDialog.value) AlertDialogC()
    SideEffect {
        readExternalStorageState.launchPermissionRequest()
    }
    val imgLauncher = rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val dataStream = context.contentResolver.openInputStream(result.data?.data!!)
            coroutineScope.launch {
                viewModel.image.value = dataStream?.readBytes()
                withContext(Dispatchers.IO) {
                    dataStream?.close()
                }
            }
        }
    }
    val rImageObserver = Observer<Bitmap> {
        imageResult.value = it
    }
    val lifecycleOwner = LocalLifecycleOwner.current


    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.colorPrimaryDark))
            .wrapContentSize(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
       Row(
           horizontalArrangement = Arrangement.SpaceEvenly,
           verticalAlignment = Alignment.CenterVertically,
           modifier = Modifier
               .fillMaxWidth()
               .height(35.dp)
       ) {
           Column(
               modifier = Modifier
                   .align(Alignment.CenterVertically)
           ) {
               Text (
                   text = "Image extrapolator",
                   color = Color.White,
                   textAlign = TextAlign.Center,
                   style = MaterialTheme.typography.button,
                   modifier = Modifier.fillMaxHeight()
               )
           }

           Button(
               onClick = {
                   when(readExternalStorageState.status) {
                       PermissionStatus.Granted -> {
                           val intent = Intent(Intent.ACTION_PICK)
                           intent.type = "image/*"
                           intent.putExtra("content", true)
                           imgLauncher.launch(intent)
                       }
                       is PermissionStatus.Denied -> {
                           openPermDialog.value = true
                       }
                   }


               } ,
               colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.colorPrimary)),
           ) {
               Text("Open Image")
           }
       }
        Spacer(modifier = Modifier
            .size(300.dp)
            .fillMaxWidth())
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.Top,
            modifier = Modifier
                .fillMaxWidth()
                .height(35.dp)
        ) {
            Button(
                onClick={
                    viewModel.extrapolateImage()
                    viewModel.resultImage.observe(lifecycleOwner, rImageObserver)
                },
                colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.colorPrimary))
            ) {
                Text("Extrapolate")
            }
        }
        Spacer(modifier = Modifier
            .size(60.dp)
            .fillMaxWidth())
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.Top,
            modifier = Modifier
                .fillMaxWidth()
                .height(35.dp)
        ) {
            Image(bitmap = imageResult.value.asImageBitmap(), contentDescription = "Result")
        }
    }
}
@ExperimentalPermissionsApi
@Preview
@Composable
fun HomeScreenPreview() {
    HomeScreen()
}
@Composable
fun AlertDialogC(){
    val context = LocalContext.current
    val color = colorResource(id = R.color.colorPrimary)
    AlertDialog(
        // on dialog dismiss we are setting
        // our dialog value to false.
        onDismissRequest = { openPermDialog.value = false },

        // below line is use to display title of our dialog
        // box and we are setting text color to white.
        title = { Text(text = "Permission was denied", color = Color.White) },

        // below line is use to display
        // description to our alert dialog.
        text = { Text("You denied permission to access your photos! \n" +
                "Open settings to reconsider your decision?", color = Color.White) },

        // in below line we are displaying
        // our confirm button.
        confirmButton = {
            // below line we are adding on click
            // listener for our confirm button.
            TextButton(
                onClick = {
                    openPermDialog.value = false
                    context.openAppSystemSettings()
                }
            ) {
                // in this line we are adding
                // text for our confirm button.
                Text("Open settings", color = Color.White)
            }
        },
        // in below line we are displaying
        // our dismiss button.
        dismissButton = {
            // in below line we are displaying
            // our text button
            TextButton(
                // adding on click listener for this button
                onClick = {
                    openPermDialog.value = false
                }
            ) {
                // adding text to our button.
                Text("Dismiss", color = Color.White)
            }
        },
        // below line is use to add background color to our alert dialog
        backgroundColor = color,

        // below line is use to add content color for our alert dialog.
        contentColor = Color.White
    )
}
@Preview
@Composable
fun AlertDialogPreview() {
    AlertDialogC()
}

@Composable
fun SettingsScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.colorPrimaryDark))
            .wrapContentSize(Alignment.Center)
    ) {

    }
}

@Preview(showBackground = true)
@Composable
fun SettingsScreenPreview() {
    SettingsScreen()
}

private fun Context.openAppSystemSettings() {
    startActivity(Intent().apply {
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        data = Uri.fromParts("package", packageName, null)
    })
}
