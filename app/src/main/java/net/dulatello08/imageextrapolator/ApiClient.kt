package net.dulatello08.imageextrapolator

import android.content.Context
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*
import javax.security.cert.CertificateException



object ApiClient {
    fun setupRetrofit(context: Context): RequestInterface {

        return Retrofit.Builder()
            .baseUrl("https://image-extrapolator.onrender.com")
            .build()
            .create(RequestInterface::class.java)
    }
}
