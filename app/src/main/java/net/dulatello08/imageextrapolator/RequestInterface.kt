package net.dulatello08.imageextrapolator

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface RequestInterface {
    @Multipart
    @POST("api/v3/extrapolateImage")
    suspend fun extrapolateImage(@Part("image") image: RequestBody) : Response<ResponseBody>
}